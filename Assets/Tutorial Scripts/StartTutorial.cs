using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class StartTutorial : MonoBehaviour
{
    // Start is called before the first frame update
    public bool startClicked = false;
    [SerializeField] private GameObject _grid;
    [SerializeField] private GameObject _avatar;
    [SerializeField] private Sprite stop;
    [SerializeField] private Sprite play;
    private TMP_Text _avatarComponent;
    private GridLayoutGroup _gridComponent;
    private List<ProgItemTigger> _triggers;
    private List<string> _collidedBlocks = new List<string>();
    private int clicked = 0;

    private static string[] orderList =
    {
        "On start(Clone)", "If(Clone)Equals(Clone)Position(Clone)Position 0(Clone)", "Give blanket(Clone)"
    };
    
    //Hardcode all posible combinations or somehow loop through one and check indexes

    //private List<List<string>> _correctOrder = new List<List<string>>({new(orderList), new(orderList2)});

    private static string[][] _correctOrder = {orderList};
    private void Awake()
    {
        _gridComponent = _grid.GetComponent<GridLayoutGroup>();
        _avatarComponent = _avatar.GetComponent<TMP_Text>();
        
    }

    public void ButtonClicked()
    {
        if (clicked == 0)
        {
            _triggers = _gridComponent.GetComponentsInChildren<ProgItemTigger>().ToList();
            if (_collidedBlocks!= null && _collidedBlocks.Any()) _collidedBlocks.Clear();
            Debug.Log("List: ");
            foreach (var trigger in _triggers)
            {
                if (_collidedBlocks != null && trigger.CollidedWith != "")
                {
                    _collidedBlocks.Add(trigger.CollidedWith);
                    Debug.Log(trigger.CollidedWith);
                }
            }
            
            if (Array.Exists(_correctOrder, element => element.SequenceEqual(_collidedBlocks)))
            {
                startClicked = true;
                _avatarComponent.text = "Congrats! You have successfully solved the task!!";
                clicked++;
                gameObject.GetComponent<Image>().sprite = stop;
            }
            else
            {
                _avatarComponent.text = "Please try again.";
            }
        }
        else if(clicked!=0 && startClicked)
        {
            clicked--;
            gameObject.GetComponent<Image>().sprite = play;
            startClicked = false;
        }
        
    }
}