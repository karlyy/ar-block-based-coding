using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTutoroial : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] GameObject[] DogArray = new GameObject[1];
    [SerializeField] private float speed = 10.0f;
    [SerializeField] private Sprite _blanket;
    [SerializeField] private Sprite _AnnaFace;
    [SerializeField] private Sprite _AnnaLeft;
    [SerializeField] private GameObject avatar;
    [SerializeField] private Button start;
    [SerializeField] GameObject points;
    private TMP_Text avatarComponent;
    private TMP_Text pointsText;
    private int NextIndex;
    private Transform NextDog;
    private Vector3 _originalPosition;
    private bool finished = false;

    private void Awake()
    {
        avatarComponent = avatar.GetComponent<TMP_Text>();
        avatarComponent.text = "Hi! Let's help my friends";
        _originalPosition = transform.position;
        pointsText = points.GetComponent<TMP_Text>();
    }
    void Start()
    {
        NextDog = DogArray[0].transform;
    }

    // Update is called once per frame
    private void Update()
    {
        StartTutorial startBtn = start.GetComponent<StartTutorial>();
        
        if (startBtn.startClicked) MoveToDog();
    }

    void MoveToDog()
    {

        if (finished)
        {
            transform.position = Vector3.MoveTowards(transform.position, _originalPosition, speed * Time.deltaTime);
            return;
        }
        if (transform.position == NextDog.position)
        {
            DogNeed(NextIndex);
            
            pointsText.text = "100";
            avatarComponent.text = "Perfect! You have earned 100 points!";
            finished = true;

        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, NextDog.position, speed * Time.deltaTime);
        }
        
    }

    void DogNeed(int dogPosition)
    {
        Image anna = GetComponent<Image>();
        
        DogArray[dogPosition].AddComponent<Image>();
        DogArray[dogPosition].GetComponent<Image>().rectTransform.sizeDelta = new Vector2(50, 50);
        TMP_Text textInside = DogArray[dogPosition].GetComponentInChildren<TMP_Text>();
        Destroy(textInside);
        
        avatarComponent.text = "The dog on position 0 is cold.";
            
        DogArray[dogPosition].GetComponent<Image>().sprite = Instantiate(_blanket);
        anna.sprite = _AnnaFace;

    }

    
    
    
}