using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAvatar3D : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] GameObject[] DogArray = new GameObject[8];
    private float speed = 0.5f;
    [SerializeField] private GameObject avatar;  //guiding avatar
    //[SerializeField] private Button start;
    //[SerializeField] private Button stop;
    private TMP_Text avatarComponent;
    DogFeel dogFeel;
    private int NextIndex;
    private Transform NextDog;
    private Vector3 _originalPosition;
    private Animator animatorAnna;

    private void Awake()
    {
        dogFeel = DogArray[0].GetComponent<DogFeel>();
        avatarComponent = avatar.GetComponent<TMP_Text>();
        avatarComponent.text = "Hi!";
        _originalPosition = transform.position;
        animatorAnna = GetComponent<Animator>();
    }
    void Start()
    {
        NextDog = DogArray[0].transform;
    }

    // Update is called once per frame
    private void Update()
    {
        //StartClicked startBtn = start.GetComponent<StartClicked>();
        //StopClicked stopBtn = stop.GetComponent<StopClicked>();
        MoveToDog();
        /*if (startBtn.startClicked) MoveToDog();
        if (stopBtn.stopClicked) return;*/
    }

    void MoveToDog()
    {
        animatorAnna.SetBool("Aiming", false);
        animatorAnna.SetFloat ("Speed", 0.5f);
        if (NextIndex == 8)
        {
            transform.position = Vector3.MoveTowards(transform.position, _originalPosition, speed * Time.deltaTime);
            return;
        }
        if (transform.position == (NextDog.position+Vector3.left) || transform.position == (NextDog.position+Vector3.back) || transform.position == (NextDog.position+Vector3.right))
        {
            Debug.Log(DogArray[NextIndex].name);
            Debug.Log(DogIsFeeling(dogFeel.Feel));

            DogNeed(dogFeel.Feel, NextIndex);
            
            NextIndex++;
            if (NextIndex==1) transform.Rotate(0, -74, 0, Space.Self);
            else if (NextIndex==3) transform.Rotate(0, -90, 0, Space.Self);
            else if (NextIndex==6) transform.Rotate(0, -90, 0, Space.Self);
             
            if (NextIndex == 8)
            {
                transform.Rotate(0, -90, 0, Space.Self);
                transform.position = Vector3.MoveTowards(transform.position, _originalPosition, speed * Time.deltaTime);
                return;
            }
            NextDog = DogArray[NextIndex].transform;
            dogFeel = DogArray[NextIndex].GetComponent<DogFeel>();
        }
        else
        {
            if (NextIndex<2) transform.position = Vector3.MoveTowards(transform.position, (NextDog.position+Vector3.left), speed * Time.deltaTime);
            if (NextIndex>=2 && NextIndex<=4) transform.position = Vector3.MoveTowards(transform.position, (NextDog.position+Vector3.back), speed * Time.deltaTime);
            if (NextIndex>4) transform.position = Vector3.MoveTowards(transform.position, (NextDog.position+Vector3.right), speed * Time.deltaTime);
        }
    }

    string DogIsFeeling(int x)
    {
        if (x == 0) return "Hungry";
        if (x == 1) return "Sad";
        if (x == 2) return "Hungry and sad";
        return "";
    }

    void DogNeed(int dogFeel, int dogPosition)
    {
        GameObject childFood = DogArray[dogPosition].transform.GetChild(0).gameObject;
        GameObject childToy = DogArray[dogPosition].transform.GetChild(1).gameObject;
        TMP_Text textInside = DogArray[dogPosition].GetComponentInChildren<TMP_Text>();
        Animator an = DogArray[dogPosition].GetComponentInParent<Animator>();
        Destroy(textInside);
        
        if (dogFeel == 0)
        {
            avatarComponent.text = "The dog on position " + dogPosition + " is hungry!";
            //PUT WAITING TIME ?
            childFood.SetActive(true);
            if(dogPosition<2 || dogPosition>5) an.Play ("Arm_Dog|Eat_loop", -1, 0f);
            else an.Play ("Eat", -1, 0f);
        }
        else if (dogFeel == 1)
        {
            avatarComponent.text = "The dog on position " + dogPosition + " is sad!";
            childToy.SetActive(true);
            if(dogPosition<2 || dogPosition>5) an.Play ("Arm_Dog|Digging_loop", -1, 0f);
            else an.Play ("Play", -1, 0f);
        }
        else 
        {
            avatarComponent.text = "The dog on position " + dogPosition + " is sad and hungry!";
            childFood.SetActive(true);
            childToy.SetActive(true);
            if(dogPosition<2 || dogPosition>5) an.Play ("Arm_Dog|Jump_Inplace_Full", -1, 0f);
            else an.Play ("Jump", -1, 0f);
        }
        
    }

    
    
    
}

