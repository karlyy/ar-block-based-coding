﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public Animator anim;
	public Rigidbody rbody;
	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator>();
		rbody = GetComponent<Rigidbody>();
		anim.Play ("Sit", -1, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		//anim.Play ("idle", -1, 0f);
	
	}
	
}
