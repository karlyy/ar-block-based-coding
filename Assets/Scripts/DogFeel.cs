using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogFeel : MonoBehaviour
{
    // Start is called before the first frame update
    public int Feel;

    void Start()
    {
        Feel = Random.Range(0,3);     //0=Hungry, 1=Sad, 2=Both
    }

    int getFeel()
    {
        return Feel;
    }
    
}
