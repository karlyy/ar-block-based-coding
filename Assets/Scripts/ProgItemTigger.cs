using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
public class ProgItemTigger : MonoBehaviour
{
    public string CollidedWith ="";
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("ON ENTER");
        Debug.Log("Item: " + gameObject.name);
        Debug.Log("Collided with:" + other.gameObject.name);
        CollidedWith += other.gameObject.name;                       //When collided, concat the name 
        //other.transform.localPosition = transform.localPosition;  //Magnet effect to be improved
    }

    private void OnTriggerStay(Collider other)
    {
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("ON EXIT");
        CollidedWith = CollidedWith.Replace(other.gameObject.name, "") ; //On exit, remove the name
        Debug.Log(CollidedWith);
        
    }

}
