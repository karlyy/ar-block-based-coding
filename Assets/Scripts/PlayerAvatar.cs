using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAvatar : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] GameObject[] DogArray = new GameObject[8];
    [SerializeField] private float speed = 10.0f;
    [SerializeField] private Sprite _toy;
    [SerializeField] private Sprite _food;
    [SerializeField] private Sprite _toyFood;
    [SerializeField] private Sprite _AnnaFace;
    [SerializeField] private Sprite _AnnaLeft;
    [SerializeField] private bool clicked;
    [SerializeField] private GameObject avatar;
    [SerializeField] private Button start;
    [SerializeField] GameObject points;
    private TMP_Text avatarComponent;
    private TMP_Text pointsText;
    DogFeel dogFeel;
    private int NextIndex;
    private Transform NextDog;
    private Vector3 _originalPosition;

    private void Awake()
    {
        dogFeel = DogArray[0].GetComponent<DogFeel>();
        avatarComponent = avatar.GetComponent<TMP_Text>();
        avatarComponent.text = "Hi! Let's help my friends";
        _originalPosition = transform.position;
        pointsText = points.GetComponent<TMP_Text>();
    }
    void Start()
    {
        NextDog = DogArray[0].transform;
    }

    // Update is called once per frame
    private void Update()
    {
        StartClicked startBtn = start.GetComponent<StartClicked>();
        /*if (NextIndex == DogArray.Length-1)
        {
            transform.position = _originalPosition;
        }
        else */
        if (startBtn.startClicked) MoveToDog();
    }

    void MoveToDog()
    {
        if (NextIndex == 8)
        {
            transform.position = Vector3.MoveTowards(transform.position, _originalPosition, speed * Time.deltaTime);
            pointsText.text = "100";
            avatarComponent.text = "Perfect! You have earned 100 points!";
            return;
        } 
        if (transform.position == NextDog.position)
        {
            //Debug.Log(DogArray[NextIndex].name);
            //Debug.Log(DogIsFeeling(dogFeel.Feel));

            DogNeed(dogFeel.Feel, NextIndex);
            
            NextIndex++;
            if (NextIndex == 8)
            {
                transform.position = Vector3.MoveTowards(transform.position, _originalPosition, speed * Time.deltaTime);
                pointsText.text = "100";
                avatarComponent.text = "Perfect! You have earned 100 points!";
                return;
            }               
            NextDog = DogArray[NextIndex].transform;
            dogFeel = DogArray[NextIndex].GetComponent<DogFeel>();
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, NextDog.position, speed * Time.deltaTime);
        }
    }

    string DogIsFeeling(int x)
    {
        if (x == 0) return "Hungry";
        if (x == 1) return "Sad";
        if (x == 2) return "Hungry and sad";
        return "";
    }

    void DogNeed(int dogFeel, int dogPosition)
    {
        Image anna = GetComponent<Image>();
        if (dogPosition == 2) anna.sprite = _AnnaLeft;
        else if (dogPosition == 5) anna.sprite = _AnnaFace;
        DogArray[dogPosition].AddComponent<Image>();
        DogArray[dogPosition].GetComponent<Image>().rectTransform.sizeDelta = new Vector2(50, 50);
        TMP_Text textInside = DogArray[dogPosition].GetComponentInChildren<TMP_Text>();
        Destroy(textInside);
        
        if (dogFeel == 0)
        {
            avatarComponent.text = "The dog on position " + dogPosition + " is hungry!";
            //PUT WAITING TIME ?
            
            DogArray[dogPosition].GetComponent<Image>().sprite = Instantiate(_food);
            return;
        }

        if (dogFeel == 1)
        {
            avatarComponent.text = "The dog on position " + dogPosition + " is sad!";
            DogArray[dogPosition].GetComponent<Image>().sprite = Instantiate(_toy);
            return;
        }

        if (dogFeel == 2)
        {
            avatarComponent.text = "The dog on position " + dogPosition + " is sad and hungry!";
            DogArray[dogPosition].GetComponent<Image>().sprite = Instantiate(_toyFood);
            return;
        }
        
    }

    
    
    
}

