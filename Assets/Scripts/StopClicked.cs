using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class StopClicked : MonoBehaviour
{
    public bool stopClicked;
    [SerializeField] private Button startBtn;
    private StartClicked start;

    private void Update()
    {
        start = startBtn.GetComponent<StartClicked>();
        if (start.startClicked) stopClicked = false;
    }

    public void ButtonClicked()
    {
        stopClicked = true;
    }
}
