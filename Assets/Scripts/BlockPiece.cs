using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BlockPiece : MonoBehaviour, IBeginDragHandler, IDragHandler,IEndDragHandler
{
    private Vector2 lastMousePosition;
    public GameObject beingDragged;
    private GameObject progField;
    public bool dropped = false;

    public bool toOrginal;

    private Vector3 orginalPosition;

    private List<Collider2D> currentColliders;

    private void Awake()
    {
        progField = GameObject.FindGameObjectWithTag("progField");
    }

    private void Start() {
        orginalPosition = transform.localPosition;
    }
    public void OnBeginDrag(PointerEventData eventData) {
        lastMousePosition = eventData.position;

        if (gameObject.transform.parent != progField.transform)
        {
            GameObject dragged = Instantiate(gameObject);
            beingDragged = dragged;
            beingDragged.transform.SetParent(progField.transform);
        }
        else
        {
            beingDragged = gameObject;
        }

    }
    
    public void OnDrag(PointerEventData eventData) {
        dropped = false;
        beingDragged.transform.position = eventData.position;
    }
    
    public void OnEndDrag(PointerEventData eventData)
    {
        if (toOrginal) transform.localPosition = orginalPosition;
        dropped = true;
    }

    /*private void OnCollisionEnter2D(Collision2D other)
    {
        if (!currentColliders.Contains(other.collider)) currentColliders.Add(other.collider);
        //transform.position = currentColliders[0].transform.position;
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (dropped)
        {
            transform.localPosition = currentColliders[0].transform.localPosition;
            Debug.Log("current colliders" + currentColliders);
            Debug.Log("collider 0 " + currentColliders[0].transform.localPosition);
            Debug.Log("collider my" + transform.localPosition);
            
        }
        

    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (currentColliders.Contains(other.collider)) currentColliders.Remove(other.collider);
    }*/

    private void OnTriggerStay2D(Collider2D other)
    {
        if (dropped)
        {
            if (transform.CompareTag("positionUp"))
            {
                transform.position = other.transform.position;
            }
            else if (transform.CompareTag("operator"))
            {
                transform.position = other.transform.position + new Vector3(0, -10, 0);
            }
            else if (transform.CompareTag("event"))
            {
                transform.position = other.transform.position + new Vector3(-8, 0, 0);
            }
            else if (transform.CompareTag("control2"))
            {
                transform.position = other.transform.position + new Vector3(8, 3, 0);
            }
            else if (transform.CompareTag("move"))
            {
                transform.position = other.transform.position + new Vector3(5, 0, 0);
            }
            else
            {
                transform.position = transform.position;
            }
            
        }
    }
}