using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Jobs;
using UnityEngine.UI;
public class SunButtons : MonoBehaviour, IPointerUpHandler
{
    [SerializeField] private Image _sunImg;
    [SerializeField] private Sprite _clicked;
    [SerializeField] private GameObject[] blocks = new GameObject[10];
    private int _blockInLeftIndex;    //index of block that is currently inside left cloud
    private int _blockInRightIndex;   //index of block that is currently inside right cloud
    private int _myLeftBlockIndex;     //index of block that needs to go in left cloud
    private int _myRightBlockIndex;     //index of block that needs to go in right cloud
    private Animator _animatorMyLeftBlock;   //animator of block that needs to come in left cloud
    private Animator _animatorMyRightBlock;   //animator of block that needs to come in right cloud
    private Animator _animatorBlockInLeft;  //animator of block that needs to go in left cloud
    private Animator _animatorBlockInRight;  //animator of block that needs to go in right cloud
    // Start is called before the first frame update

    private void Awake()
    {
        goThroughBlocks();
    }

    // Update is called once per frame
    void Update()
    {
        goThroughBlocks();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _sunImg.sprite = _clicked;
        //_animatorBlockIn.SetTrigger("Out");
        //_animatorMyBlock.SetTrigger("In");
        _animatorBlockInLeft.SetInteger("value", 0);
        _animatorBlockInRight.SetInteger("value", 0);
        _animatorMyLeftBlock.SetInteger("value", 1);
        _animatorMyRightBlock.SetInteger("value", 1);
        //goThroughBlocks();
        Debug.Log(_clicked.name + " clicked");
        
    }

    private void goThroughBlocks()
    {
        for (int i = 0; i < blocks.Length; i++)
        {
            if (transform.tag == blocks[i].name) _myLeftBlockIndex = i;     //connecting button to corresponding block
            else if (transform.tag+"2" == blocks[i].name) _myRightBlockIndex = i;
            if ( blocks[i].transform.parent.name == "Cloud1" && blocks[i].GetComponent<RectTransform>().anchoredPosition == Vector2.zero) _blockInLeftIndex = i;  //finding out which block is already in 
            else if ( blocks[i].transform.parent.name == "Cloud2" && blocks[i].GetComponent<RectTransform>().anchoredPosition == Vector2.zero) _blockInRightIndex = i;
        }

        _animatorMyLeftBlock = blocks[_myLeftBlockIndex].GetComponent<Animator>();
        _animatorMyRightBlock = blocks[_myRightBlockIndex].GetComponent<Animator>();
        _animatorBlockInLeft = blocks[_blockInLeftIndex].GetComponent<Animator>();
        _animatorBlockInRight = blocks[_blockInRightIndex].GetComponent<Animator>();
    }
}
